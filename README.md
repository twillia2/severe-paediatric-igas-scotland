# Severe paediatric invasive group A streptococcal disease in Scotland

## Description
Supporting materials for national cohort study looking at the burden of severe paediatric invasive group A streptococcal disease (iGAS) in Scotland. 

## Support
Please contact thomas.christie.williams@ed.ac.uk if you have any questions about this repository. 
